import requests
import requests.exceptions
from urllib3 import exceptions as urlexcept
import socket
from time import sleep
import multiprocessing.pool as mp
from itertools import product
import dill


# Set Riot API key to use.
api_key = 'RGAPI-915c3195-a0fd-4d14-b4df-73bf9c8f08fa'


# Set regions, queue type and ID, tiers, divisions, and season to scrape data from.
regions = ['na1', 'kr', 'euw1', 'eun1', 'jp1', 'br1', 'ru', 'la1', 'la2']
queues = ['RANKED_SOLO_5x5']
queue_ids = [420]
tiers = ['DIAMOND', 'PLATINUM', 'GOLD']
divs = ['I', 'II', 'III', 'IV']
seasons = ['13']


# Create queue set for Player IDs and Match IDs for each region.
PID_queue = {region: set() for region in regions}
MID_queue = {region: set() for region in regions}


# Set up starting database to record synergy/counter wins and losses.
db = {}


# Scrapes a single call to API; data type is indicated by key; data contains unique request.
def scrape(key, region, data, queue_id=420, api=api_key):

    # Create dictionary to dispatch the request by key.
    data_dict = {
        'summoner': f'summoner/v4/summoners/{data}?api_key={api}',
        'matchlist': f'match/v4/matchlists/by-account/{data}?queue={queue_id}&api_key={api}',
        'match': f'match/v4/matches/{data}?api_key={api}',
        'league': f'league/v4/entries/{data}?api_key={api}',
        'GM_league': f'league/v4/{data}?api_key={api}'
    }

    # Add the request to base URL.
    url = f'https://{region}.api.riotgames.com/lol/' + data_dict[key]

    # Send request to Riot API.
    try:
        response = requests.get(url, timeout=5)

    # Catch websocket exceptions.
    except socket.gaierror or socket.error:
        print(f'Address error: pausing thread {region}.')
        sleep(10)
        return None

    # Catch maximum requests exceeded exceptions.
    except urlexcept.MaxRetryError:
        print(f'Max retries exceeded. pausing thread {region}.')
        sleep(10)
        return None

    # Catch URL connection errors.
    except urlexcept.NewConnectionError or urlexcept.ConnectionError:
        print(f'Connection error: pausing thread {region}')
        sleep(10)
        return None

    # Catch HTTP errors.
    except requests.exceptions.HTTPError:
        print(f'HTTP error: pausing thread {region}')
        sleep(10)
        return None

    # Pause for rate limits if encountered.
    if response.status_code == 429:
        print(response.status_code, f'Rate limit exceeded: pausing thread {region}.')

        # Attempt to get pause length from response header.
        try:
            sleep(float(response.headers['Retry-After']))

        # Sleep for 45s if unable to get pause length.
        except KeyError:
            sleep(45)

        # TODO: recursion to reattempt request after thread pause.
        finally:
            return None

    # If exception is not encountered, handle returned data.
    else:

        # If bad response, no data is returned.
        if not response.ok:
            print(f'No data retrieved: {response.status_code} from {region}.')
            return None

        # If good (200) response, return the data as JSON.
        else:
            print(f'Success: gathered {key} from {region}')
            sleep(0.5)
            return response.json()


# Scrape the player IDs from all specified queues, tiers, and divisions from a single region.
def get_league(region):

    for queue, tier, div in product(queues, tiers, divs):

        # Handle API requests for high-elo leagues.
        if tier in ['CHALLENGER', 'GRANDMASTER', 'MASTER']:
            league = scrape('GM_league', region, f'{tier.lower()}leagues/by-queue/{queue}')

            # If data is successfully returned; add summoner IDs to Player ID queue set.
            if league:
                PID_queue[region] |= {entry['summonerId'] for entry in league['entries'] if league}

        # Handle API requests for all other tiers.
        else:
            league = scrape('league', region, f'{queue}/{tier}/{div}')

            # If data is successfully returned; add summoner IDs to Player ID queue set.
            if league:
                PID_queue[region] |= {entry['summonerId'] for entry in league if league}

        # Save the Player ID queue for each queue/tier/division scraped.

        # with open(r'C:\Users\Drew\Documents\patch_6_summs.pkl', 'wb') as b:
            # dill.dump(PID_queue, b)


def get_matches(region):
    """Gathers Match IDs and adds them to the region's list."""

    for summ in PID_queue[region]:
        summoner = scrape('summoner', region, summ)
        if summoner:
            for queue_id in queue_ids:
                MID = scrape('matchlist', region, summoner['accountId'], queue_id=queue_id)
                if MID:
                    MID_queue[region] |= {match['gameId'] for match in MID['matches']}

        # with open(r'C:\Users\Drew\Documents\patch_6_matches.pkl', 'wb') as b:
            # dill.dump(MID_queue, b)


def get_match_data(region):
    """Gathers data from each Match ID and extracts Champion/Team/Win data."""

    data_dict = {True: 'W', False: 'L'}
    for match in MID_queue[region]:
        m = scrape('match', region, match)
        if m:
            results = [(summoner['championId'], summoner['stats']['win']) for summoner in m['participants']]
            for champ in results:
                name, win = champ
                key = data_dict[win]
                for other in results:
                    other_name, other_win = other
                    if other_name == name:
                        continue
                    elif other_win == win:
                        db[str(name)]['synergies'][str(other_name)][key] += 1
                    else:
                        db[str(name)]['counters'][str(other_name)][key] += 1
            with open(r'C:\Users\Drew\Documents\patch_6_database.pkl', 'wb') as f:
                dill.dump(db, f)
            print(f'Match data added for {match} in {region}')


def gather_summoners():
    """Gathers data from Riot API and stores it in dataset."""

    with mp.ThreadPool(processes=len(regions)) as pool:
        print('*** Started regional league scraping threadpool. ***')
        pool.map(get_league, regions)
        print('*** All league\'s data gathered.')


def gather_matches():
    """Gathers matchlists from Riot API and stores them in dataset."""

    with mp.ThreadPool(processes=len(regions)) as pool:
        print('*** Started regional match scraping threadpool. ***')
        pool.map(get_matches, regions)
        print('*** Retrieved matchlists. ***')


def gather_match_data():
    """Gathers match data from Riot API and stores in champion dataset."""

    with mp.ThreadPool(processes=len(regions)) as pool:
        print('*** Started extracting match data. ***')
        pool.map(get_match_data, regions)
        print('*** Finished data Extraction. ***')


if __name__ == '__main__':
    gather_summoners()
    gather_matches()
    gather_match_data()
